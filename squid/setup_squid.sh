#!/bin/bash
#================================================================
# HEADER
#================================================================
# SYNOPSIS
#    setup_squid.sh
#    (no parameters)
# DESCRIPTION
#    squid setup script for RHEL7.
# OPTIONS
#    (no options)
# EXAMPLES
#    setup_squid.sh
#
#================================================================
# IMPLEMENTATION
#    version         setup_squid.sh 1.00
#    author          S.Sasaki(sasaki@cns.co.jp)
#    copyright       Copyright 2016 CNS Co.,Ltd. http://www.cns.co.jp/
#    license         GNU General Public License
#
#================================================================
#  HISTORY
#     2016/09/28 : sasaki : Script creation
# 
#================================================================


yum -y install squid
cp -p /etc/squid/squid.conf /etc/squid/squid.conf.ORG

cat <<EOL > /etc/squid/squid.conf
# アクセスを許可するネットワーク(LANだけ)
acl localnet src 192.168.0.0/16

# アクセスを許可するポート
acl SSL_ports port 443
acl Safe_ports port 80          # http
acl Safe_ports port 21          # ftp
acl Safe_ports port 443         # https
acl Safe_ports port 70          # gopher
acl Safe_ports port 210         # wais
acl Safe_ports port 1025-65535  # unregistered ports
acl Safe_ports port 280         # http-mgmt
acl Safe_ports port 488         # gss-http
acl Safe_ports port 591         # filemaker
acl Safe_ports port 777         # multiling http
acl CONNECT method CONNECT

#
# Access Control
#
# 安全じゃないポートを拒否
http_access deny !Safe_ports
# LAN内のアクセスを許可
http_access allow localnet
# 上記以外は全て拒否
http_access deny all

# ホスト名
visible_hostname proxy01

# Squid normally listens to port 3128
http_port 8080

# Uncomment and adjust the following to add a disk cache directory.
#cache_dir ufs /var/spool/squid 100 16 256

# Leave coredumps in the first cache dir
coredump_dir /var/spool/squid

#
# Add any of your own refresh_pattern entries above these.
#
refresh_pattern ^ftp:           1440    20%     10080
refresh_pattern ^gopher:        1440    0%      1440
refresh_pattern -i (/cgi-bin/|\?) 0     0%      0
refresh_pattern .               0       20%     4320

# プロキシ経由でアクセスしていることをアクセス先に知られないようにする
request_header_access X-Forwarded-For deny all
request_header_access Via deny all
request_header_access Cache-Control deny all
EOL

systemctl enable squid
systemctl start squid
